import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Paul
 * on 12.12.2018
 *
 * @author pauhull
 */
public class ObfGenerator {

    private static Random random = new Random();

    public static void main(String[] args) throws IOException {

        File file = new File("E:/obf.txt");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(writer);

        List<String> generatedLines = new ArrayList<>();
        int lineCount = 0;
        do {
            StringBuilder generatedLine = new StringBuilder();
            for (int i = 0; i < 200; i++) {
                generatedLine.append(random.nextBoolean() ? "I" : "l");
            }
            if (!generatedLines.contains(generatedLine.toString())) {
                printWriter.println(generatedLine.toString());
                generatedLines.add(generatedLine.toString());
                lineCount++;
            }
        } while (lineCount < 300);

        printWriter.close();
        writer.close();
        System.exit(0);
    }

}
