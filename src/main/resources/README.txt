Urheberrechtshinweis
Copyright © Paul H.
Kompiliert am: ${timestamp}

Alle Inhalte des Quelltextes des Plug-Ins "PaintWars" sind urheberrechtlich geschützt.
Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet,
bei Paul H. Alle Rechte vorbehalten.

Jede Art der Vervielfältigung, Verbreitung, Vermietung, Verleihung
öffentlich Zugänglichmachung oder anderer Nutzung
bedarf der ausdrücklichen, schriftlichen Zustimmung von Paul H.

Der Quellcode des Plug-Ins ist mit dem Programm ProGuard (https://www.guardsquare.com/en/products/proguard)
verschlüsselt ("obfuscated") und ein Dekompilieren des Plug-Ins ist ohne die schriftliche Einwilligung des
Autors (Paul H.) untersagt.