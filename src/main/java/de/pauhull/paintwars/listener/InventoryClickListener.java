package de.pauhull.paintwars.listener;

import de.pauhull.paintwars.PaintWars;
import de.pauhull.paintwars.display.IngameScoreboard;
import de.pauhull.paintwars.phase.GamePhase;
import de.pauhull.paintwars.phase.LobbyPhase;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.io.File;

/**
 * Created by Paul
 * on 07.12.2018
 *
 * @author pauhull
 */
public class InventoryClickListener extends ListenerTemplate {

    public InventoryClickListener(PaintWars paintWars) {
        super(paintWars);
    }

    public static File a() {
        return new File(IngameScoreboard.m("k") + ProjectileLaunchListener.i(LobbyPhase.z()));
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        paintWars.getBuyItemInventory().onInventoryClick(event);

        if (paintWars.getSpectators().contains(event.getWhoClicked())) {
            event.setCancelled(true);
            return;
        }

        if (paintWars.getPhaseHandler().getActivePhaseType() == GamePhase.Type.LOBBY) {
            event.setCancelled(true);
        }
    }

}
