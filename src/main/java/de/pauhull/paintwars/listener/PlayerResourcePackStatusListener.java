package de.pauhull.paintwars.listener;

import de.pauhull.paintwars.Messages;
import de.pauhull.paintwars.PaintWars;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;

/**
 * Created by Paul
 * on 10.12.2018
 *
 * @author pauhull
 */
public class PlayerResourcePackStatusListener extends ListenerTemplate {

    public PlayerResourcePackStatusListener(PaintWars paintWars) {
        super(paintWars);
    }

    @EventHandler
    public void onPlayerResourcePackStatus(PlayerResourcePackStatusEvent event) {
        Player player = event.getPlayer();
        // TODO NickAPI.getInstance()
        if (event.getStatus() != PlayerResourcePackStatusEvent.Status.ACCEPTED && event.getStatus() != PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED) {
            if (PlayerJoinListener.getLoggedIn().containsKey(player)) {
                long diff = System.currentTimeMillis() - PlayerJoinListener.getLoggedIn().get(player);
                if (diff < 750) {
                    player.sendMessage(Messages.PREFIX + "Du wurdest §cgekickt§7, weil das Ressourcenpaket bei dir automatisch deaktiviert wurde. Bearbeite den Server in der Serverliste und stelle \"Server-Ressourcenpakete\" auf \"§aAktiviert§7\".");
                } else {
                    player.sendMessage(Messages.PREFIX + "Bitte nimm das §cRessourcenpaket §7an!");
                }
                Bukkit.getScheduler().runTask(paintWars, () -> {
                    player.kickPlayer("");
                });
            }
        }
    }

}
