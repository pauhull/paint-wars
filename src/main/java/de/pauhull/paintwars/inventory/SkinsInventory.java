package de.pauhull.paintwars.inventory;

import de.pauhull.paintwars.Messages;
import de.pauhull.paintwars.PaintWars;
import de.pauhull.paintwars.game.Skin;
import de.pauhull.paintwars.util.ItemBuilder;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Paul
 * on 07.12.2018
 *
 * @author pauhull
 */
public class SkinsInventory implements Listener {

    private static final String TITLE = "§cWaffenskin ändern";
    private static final ItemStack BLACK_GLASS = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setDisplayName(" ").build();

    @Getter
    private static final Map<Player, Skin> selectedSkins = new HashMap<>();

    private PaintWars paintWars;


    public SkinsInventory(PaintWars paintWars) {
        this.paintWars = paintWars;

        Bukkit.getPluginManager().registerEvents(this, paintWars);
    }

    public void show(Player player) {

        Skin[] skins = Skin.values();

        Inventory inventory = Bukkit.createInventory(null, 18 + (int) Math.ceil(skins.length / 9.0) * 9, TITLE);

        for (int i = 0; i < inventory.getSize(); i++) {
            if (i < 9 || i > inventory.getSize() - 10) {
                inventory.setItem(i, BLACK_GLASS);
            }
        }

        final AtomicInteger loadedSkins = new AtomicInteger();
        paintWars.getSkinsTable().getSelected(player.getUniqueId(), selected -> {
            for (Skin skin : skins) {
                paintWars.getSkinsTable().hasSkin(player.getUniqueId(), skin.toString(), hasSkin -> {
                    Bukkit.getScheduler().runTask(paintWars, () -> {
                        ItemStack stack = hasSkin ? skin.getItemBought() : skin.getItem();

                        if (skin.toString().equals(selected)) {
                            stack = new ItemBuilder(stack.clone()).addEnchant(Enchantment.DURABILITY, 0, true).addItemFlag(ItemFlag.HIDE_ENCHANTS).build();
                        }

                        int slot = Arrays.asList(Skin.values()).indexOf(skin) + 9;
                        inventory.setItem(slot, stack);

                        if (loadedSkins.incrementAndGet() >= Skin.values().length) {
                            Bukkit.getScheduler().runTask(paintWars, () -> {
                                player.playSound(player.getLocation(), Sound.ITEM_PICKUP, 1, 1);
                                player.openInventory(inventory);
                            });
                        }
                    });
                });
            }
        });
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getClickedInventory();
        ItemStack stack = event.getCurrentItem();

        if (inventory == null || inventory.getTitle() == null || !inventory.getTitle().equals(TITLE)) {
            return;
        } else {
            event.setCancelled(true);
        }

        paintWars.getSkinsTable().getSelected(player.getUniqueId(), selected -> {
            if (stack != null) {
                for (Skin skin : Skin.values()) {
                    if (!stack.getEnchantments().isEmpty()) {
                        player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
                        player.sendMessage(Messages.PREFIX + "Du hast diesen §cSkin §7bereits ausgewählt!");
                        return;
                    } else if (stack.equals(skin.getItemBought())) {
                        player.playSound(player.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
                        player.sendMessage(Messages.PREFIX + "Skin §aerfolgreich §7ausgewählt!");
                        player.closeInventory();
                        selectedSkins.put(player, skin);

                        paintWars.getSkinsTable().hasSkin(player.getUniqueId(), skin.toString(), hasSkin -> {
                            if (!hasSkin) {
                                paintWars.getSkinsTable().addSkin(player.getUniqueId(), skin.toString());
                            }
                            paintWars.getSkinsTable().select(player.getUniqueId(), skin.toString());
                        });

                        return;
                    } else if (stack.equals(skin.getItem())) {
                        Bukkit.getScheduler().runTask(paintWars, () -> {
                            paintWars.getBuyItemInventory().show(player, skin);
                        });
                    }
                }
            }
        });
    }

}
