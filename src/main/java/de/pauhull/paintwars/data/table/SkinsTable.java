package de.pauhull.paintwars.data.table;

import de.pauhull.paintwars.data.MySQL;
import de.pauhull.paintwars.game.Skin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class SkinsTable {

    private static final String TABLE = "paintwars_skins";

    private MySQL mySQL;
    private ExecutorService executorService;

    public SkinsTable(MySQL mySQL, ExecutorService executorService) {
        this.mySQL = mySQL;
        this.executorService = executorService;

        mySQL.update("CREATE TABLE IF NOT EXISTS `" + TABLE + "` (`id` INT AUTO_INCREMENT, `uuid` VARCHAR(255), `skin` VARCHAR(255), `active` BOOLEAN, PRIMARY KEY (`id`))");
    }

    public void select(UUID uuid, String skin) {
        hasSkin(uuid, skin, hasSkin -> {

            if (hasSkin) {
                mySQL.update("UPDATE `" + TABLE + "` SET `active`=FALSE WHERE `uuid`='" + uuid + "'");
                mySQL.update("UPDATE `" + TABLE + "` SET `active`=TRUE WHERE `uuid`='" + uuid + "' AND `skin`='" + skin + "'");
            }

        });
    }

    public void getSelected(UUID uuid, Consumer<String> consumer) {
        executorService.execute(() -> {
            try {

                ResultSet result = mySQL.query("SELECT * FROM `" + TABLE + "` WHERE `uuid`='" + uuid + "' AND `active`=TRUE");
                if (result.next()) {
                    consumer.accept(result.getString("skin"));
                    return;
                }

                consumer.accept(Skin.DEFAULT.name());

            } catch (SQLException e) {
                e.printStackTrace();
                consumer.accept(Skin.DEFAULT.name());
            }
        });
    }

    public void hasSkin(UUID uuid, String skin, Consumer<Boolean> consumer) {
        executorService.execute(() -> {
            try {

                ResultSet result = mySQL.query("SELECT * FROM `" + TABLE + "` WHERE `uuid`='" + uuid.toString() + "' AND `skin`='" + skin + "'");
                consumer.accept(result.next());

            } catch (SQLException e) {
                e.printStackTrace();
                consumer.accept(false);
            }
        });
    }

    public void addSkin(UUID uuid, String skin) {
        hasSkin(uuid, skin, hasSkin -> {
            if (!hasSkin) {
                mySQL.update("INSERT INTO `" + TABLE + "` VALUES (0, '" + uuid.toString() + "', '" + skin + "', FALSE)");
            }
        });
    }

    public void removeSkin(UUID uuid, String skin) {
        executorService.execute(() -> {
            mySQL.update("DELETE FROM `" + TABLE + "` WHERE `uuid`='" + uuid.toString() + "' AND `skin`='" + skin + "'");
        });
    }

}