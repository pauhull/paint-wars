package de.pauhull.paintwars;

/**
 * Created by Paul
 * on 06.12.2018
 *
 * @author pauhull
 */
public class Messages {

    public static String PREFIX = "§ePaintWars §8» §7";
    public static String NO_PERMISSIONS = "Dafür hast du §ckeine §7Rechte!";
    public static String ONLY_PLAYERS = "Nur §cSpieler §7können diesen Befehl benutzen!";
    public static String NOT_ONLINE = "Dieser Spieler ist §cnicht §7online!";

}
