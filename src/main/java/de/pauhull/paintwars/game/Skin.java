package de.pauhull.paintwars.game;

import de.pauhull.coins.spigot.buyable.SpigotBuyable;
import de.pauhull.paintwars.Messages;
import de.pauhull.paintwars.PaintWars;
import de.pauhull.paintwars.util.ItemBuilder;
import lombok.Getter;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.function.Consumer;

/**
 * Created by Paul
 * on 10.12.2018
 *
 * @author pauhull
 */
public enum Skin implements SpigotBuyable {

    DEFAULT(DyeColor.BLACK, "§8⬛ §cSchwert §8«", 0, false),
    BASEBALL_BAT(DyeColor.GREEN, "§8⬛ §cBaseball Schläger §8«", 2000, false),
    KATANA(DyeColor.BROWN, "§8⬛ §cKatana §8«", 2500, false),
    HAMMER(DyeColor.CYAN, "§8⬛ §cHammer §8«", 4000, false),
    WAND(DyeColor.SILVER, "§8⬛ §cZauberstab §8«", 6000, false),
    DIAMOND_SWORD(DyeColor.PURPLE, "§8⬛ §cDiamantschwert §8«", 10000, false),
    LIGHT_SABER(DyeColor.BLUE, "§8⬛ §cLichtschwert §8«", 0, true);

    @Getter
    private ItemStack item;

    @Getter
    private ItemStack itemBought;

    @Getter
    private DyeColor color;

    @Getter
    private String displayName;

    @Getter
    private int cost;

    @Getter
    private boolean premium;

    Skin(DyeColor color, String displayName, int cost, boolean premium) {
        PaintWars.getInstance().getBuyItemInventory().register(this);

        this.color = color;
        this.displayName = displayName;
        this.cost = cost;
        this.premium = premium;

        if (premium) {
            this.item = new ItemBuilder(Material.INK_SACK, 1, color.getDyeData()).setDisplayName(displayName).setLore(" ", "§6§lPREMIUM", " ").build();
            this.itemBought = item.clone();
        } else {
            if (cost == 0) {
                this.item = new ItemBuilder(Material.INK_SACK, 1, color.getDyeData()).setDisplayName(displayName).setLore(" ", "§a§lKOSTENLOS", " ").build();
                this.itemBought = item.clone();
            } else {
                this.item = new ItemBuilder(Material.INK_SACK, 1, color.getDyeData()).setDisplayName(displayName).setLore(" ", "§eKosten §8» §7" + NumberFormat.getInstance(Locale.GERMAN).format(cost), " ").build();
                this.itemBought = new ItemBuilder(item.clone()).setLore(" ", "§eKosten §8» §7" + NumberFormat.getInstance(Locale.GERMAN).format(cost), " ", "§a§lGEKAUFT", " ").build();
            }
        }
    }

    @Override
    public void onCancel(Player player) {
        PaintWars.getInstance().getSkinsInventory().show(player);
    }

    @Override
    public void onBuy(Player player) {
        PaintWars.getInstance().getSkinsTable().addSkin(player.getUniqueId(), toString());
        player.sendMessage(Messages.PREFIX + "Du hast dir diesen Skin §aerfolgreich §7gekauft!");
        player.closeInventory();
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
    }

    @Override
    public void onNotEnoughCoins(Player player) {
        player.sendMessage(Messages.PREFIX + "Dafür hast du nicht genug §cCoins§7!");
        player.playSound(player.getLocation(), Sound.BAT_DEATH, 1, 1);
        player.closeInventory();
    }

    @Override
    public void onAlreadyBought(Player player) {
        player.sendMessage(Messages.PREFIX + "Du §chast§7 diesen Skin bereits!");
        player.closeInventory();
    }

    @Override
    public void hasBought(Player player, Consumer consumer) {
        PaintWars.getInstance().getSkinsTable().hasSkin(player.getUniqueId(), toString(), consumer);
    }

    @Override
    public String getName() {
        return displayName;
    }

    @Override
    public String toString() {
        return name();
    }

}
